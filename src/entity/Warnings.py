class Warnings:
    """A collector for all warnings that may happen during the creation of schema and html.
    Use add() to append a warning, use report() to get a string of all warnings"""

    def __init__(self, raw_data):
        self.raw_data = raw_data
        self.warnings = {"table": [], "wrapper": [], "json": []}

    def add(self, source, error, severity):
        """adds a warning
        source = table, wrapper or json
        error = any string
        severity = normal or severe
        """
        if severity == "normal":
            self.warnings[source].append(error)
        elif severity == "severe":
            print("SEVERE WARNING DETECTED")
            print("Error: ", error)
            print("Quitting program!")
            quit()

    def report(self):
        """Returns a string with all warnings, formated for file-writing"""
        content = self.raw_data["name"]+" report:\n"
        for category in self.warnings:
            if len(self.warnings[category]) >= 1:
                content = content + category+":\n"
                for warning in self.warnings[category]:
                    content = content + warning+"\n"
        return content
