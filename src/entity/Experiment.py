
import re
import sys
import numpy as np
import toml

import src.boundary.read_write as io
import src.control.misc as misc
import src.control.Spider as Spider


class Experiment():
    def __init__(self, name, science_branch, mission):
        self.ftp_base_path = "ftp://npsa01.esac.esa.int"
        self.name = name
        self.urlsafename = ""
        self.landingpage = ""
        self.versions = []
        self.paths = []
        self.science_branch = science_branch
        self.mission = mission
        self.publication_date = ""
        self.start_time = ""
        self.stop_time = ""

        self.short_description = ""
        self.description = ""

        self.instrument_name = ""

        self.mission_description = ""

        self.creator_contact = ""

        self.meta_data_dict = dict()

        self.crawled = False
        self.clean_crawl = True

        self.file_dirs = {
            'DATASET.CAT': False,
            'MISSION.CAT': False,
            'EARTH_MISSION.CAT': False,
            'ROSETTA.CAT': False
        }

    def init(self, spider, science_branch):
        self.prepare_crawling(spider, science_branch)
        self.crawl_attributes(spider)
        self.check_and_write()

    def remove_old_experiments(self):
        latest_version_indice = self.get_highest_version_index(self.versions)
        self.paths = self.paths[latest_version_indice]
        self.versions = self.versions[latest_version_indice]

    def get_highest_version_index(self, list_of_versions):
        """list of versions should look like this: ['-V1.0', '-V2.0']"""
        detected_versions = []
        # 1. cast to float
        exp = re.compile(r"(?<=V).*")
        for version in list_of_versions:
            match = re.search(exp, version)
            if match:
                detected_versions.append(float(match.group()))
            else:
                print(
                    "No version number found, can't decide which is the latest data! Quitting!")
                quit()
        assert len(detected_versions) == len(
            list_of_versions), "Lost versions when casting to float!"

        # and finally, return index of highest value!
        return np.argmax(detected_versions)

    def set_file_dirs(self, found_file_dirs):
        # replaces "False" with the dir
        for file in found_file_dirs:
            self.file_dirs[file] = found_file_dirs[file]

    def append_catalog(self):
        """simply appends '/catalog to a directory to step into the catalog dir! """
        self.paths = self.paths+"/CATALOG"

    def remove_catalog(self):
        self.paths = self.paths.replace("/CATALOG", "")

    def log_unavailable_data(self, file_dirs):
        # TODO log with warning module
        if not file_dirs["DATASET.CAT"]:
            self.clean_crawl = False
            print("No dataset file found for", self.name, self.paths)
        if (not file_dirs["MISSION.CAT"]) and (not file_dirs["EARTH_MISSION.CAT"]) and (not file_dirs["ROSETTA.CAT"]):
            self.clean_crawl = False
            print("No mission file found for", self.name, self.paths)

    def set_browser_path(self):
        if type(self.paths) == str:
            self.browser_path = self.ftp_base_path+self.paths
        else:
            print("Can't create proper url, exiting! (maybe self paths is not a string?)")
            exit()

    def to_dict(self):
        all_attributes = {}
        all_attributes["name"] = self.name
        all_attributes["version"] = self.versions[1:]
        all_attributes["datePublished"] = self.publication_date
        all_attributes["creatorContact"] = self.creator_contact
        all_attributes["temporalCoverage"] = str(
            self.start_time)+" - "+str(self.stop_time)
        all_attributes["url"] = self.browser_path
        all_attributes["description"] = self.description
        all_attributes["abstract"] = self.short_description
        all_attributes["instrument"] = self.instrument_name
        all_attributes["mission"] = self.mission
        all_attributes["missionDescription"] = self.mission_description
        all_attributes["urlsafename"] = self.urlsafename
        all_attributes["landingpage"] = self.landingpage
        return all_attributes

    def get_url_safe_name(self):
        return self.name.replace(
            " ", "").replace(".", "").replace("/", "")

    def to_toml_string(self, dictionary):
        return toml.dumps(dictionary)

    def get_publication_date(self, content):
        try:
            return content["DATA_SET"]["DATA_SET_INFORMATION"]["DATA_SET_RELEASE_DATE"]
        except:
            self.clean_crawl = False
            print("No publication date found for", self.name, self.paths)
            print("content is:", content)

    def get_start_time(self, content):
        try:
            return content["DATA_SET"]["DATA_SET_INFORMATION"]["START_TIME"]
        except:
            # self.clean_crawl=False
            print("No start time date found for", self.name, self.paths)

    def get_stop_time(self, content):
        try:
            return content["DATA_SET"]["DATA_SET_INFORMATION"]["STOP_TIME"]
        except:
            # self.clean_crawl=False
            print("No stop time date found for", self.name, self.paths)

    def get_short_description(self, content):
        try:
            return content["DATA_SET"]["DATA_SET_INFORMATION"]["ABSTRACT_DESC"]
        except:
            # self.clean_crawl=False
            print("No short description found for", self.name, self.paths)

    def get_description(self, content):
        try:
            return content["DATA_SET"]["DATA_SET_INFORMATION"]["DATA_SET_DESC"]
        except:
            self.clean_crawl = False
            print("No description found for", self.name, self.paths)

    def get_mission_overview(self, content):
        try:
            return content["MISSION"]["MISSION_INFORMATION"]["MISSION_DESC"]
        except:
            # self.clean_crawl=False
            print("No mission overview found for", self.name, self.paths)

    def prepare_crawling(self, spider, science_branch):
        if type(self.paths) == list:
            self.remove_old_experiments()
        if self.readme_exists(spider):
            self.set_creator_contact()
        if not self.paths.endswith("CATALOG"):
            self.append_catalog()
        self.science_branch = science_branch
        self.set_file_dirs(spider.get_file_dirs(self.paths))
        self.log_unavailable_data(self.file_dirs)

    def readme_exists(self, spider):
        file_dirs = spider.get_file_dirs(self.paths)
        is_read_me = [f.endswith("README.TXT") for f in file_dirs]
        if sum(is_read_me) >= 1:
            return True
        else:
            return False

    def crawl_attributes(self, spider):
        if self.file_dirs["DATASET.CAT"]:
            self.set_dataset_attributes(spider)
        elif self.file_dirs["ASPERA4_SWM.CAT"]:
            self.set_dataset_attributes(spider)
        elif self.file_dirs["SPICEDS.CAT"]:
            self.set_dataset_attributes(spider)

        if self.file_dirs["MISSION.CAT"]:
            parsed_content = spider.parse_cat(
                spider.get_file(self.file_dirs["MISSION.CAT"]))
            self.set_mission_attributes(parsed_content)
        elif self.file_dirs["ROSETTA.CAT"]:
            parsed_content = spider.parse_cat(
                spider.get_file(self.file_dirs["ROSETTA.CAT"]))
            self.set_mission_attributes(parsed_content)
        elif self.file_dirs["EARTH_MISSION.CAT"]:
            parsed_content = spider.parse_cat(
                spider.get_file(self.file_dirs["EARTH_MISSION.CAT"]))
            self.set_mission_attributes(parsed_content)

    def set_dataset_attributes(self, spider):
        parsed_content = spider.parse_cat(
            spider.get_file(self.file_dirs["DATASET.CAT"]))
        self.publication_date = self.get_publication_date(parsed_content)
        self.start_time = self.get_start_time(parsed_content)
        self.stop_time = self.get_stop_time(parsed_content)
        self.short_description = self.get_short_description(parsed_content)
        self.description = self.get_description(parsed_content)

        self.urlsafename = self.get_url_safe_name()
        self.landingpage = "https://archives.esac.esa.int/doi/html/data/" + \
            self.science_branch+"/"+self.mission+"/"+self.urlsafename+".html"

    def set_mission_attributes(self, parsed_content):
        self.mission_description = self.get_mission_overview(parsed_content)

    def set_creator_contact(self):
        self.creator_contact = self.ftp_base_path+self.paths+"/AAREADME.TXT"

    def get_toml_conversion(self):
        self.remove_catalog()
        self.set_browser_path()
        self.meta_data_dict = self.to_dict()
        return self.to_toml_string(self.meta_data_dict)

    def check_and_write(self):
        path = "resources/toml/"+self.science_branch + "/" + self.mission + "/"
        misc.ensure_write_safety(path)

        if self.clean_crawl == True:
            toml = self.get_toml_conversion()
            io.write(toml, path+self.name+".toml")
            self.crawled = True
