import pickle
import pprint


class ExpChecklist:
    def __init__(self, mission_name):
        # dict of instruments, containing a list of exps
        self.collection = {}
        self.mission_name = mission_name

    def load(self):
        self.collection = pickle.load(
            open("resources/checklists/"+self.mission_name, "rb"))

    def save(self):
        pickle.dump(self.collection, open(
            "resources/checklists/"+self.mission_name, "wb"))

    def lengthyprint(self):
        print("the "+self.mission_name+" collection includes the following:")
        for instrument in self.collection:
            print(instrument, ":")
            for exp in self.collection[instrument]:
                print("\t\t", exp.name, exp.paths,
                      exp.versions, exp.crawled)
