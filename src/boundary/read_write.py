import os
import src.entity.MetaData as MetaData


def read(path):
    with open(path, 'r', encoding="utf-8") as f:
        return f.read()


def write(content, filepath):
    my_f = open(filepath, "w", encoding="utf-8")
    my_f.write(content)
    my_f.close()
    print("Wrote the file to", filepath)


def read_toml(path):
    raw_data = MetaData.MetaData(read(path)).data
    return raw_data


def get_all_file_paths(my_path):
    return [my_path+f for f in os.listdir(my_path) if os.path.isfile(
        os.path.join(my_path, f))]
    # r=root, d=directories, f = files
    # source: https://www.mkyong.com/python/python-how-to-list-all-files-in-a-directory/
    # return files


def get_all_docs(paths):
    """use with output of get_all_file_paths to read all toml files that are in subdirs of the base path"""
    documents = []
    for file_path in paths:
        raw_data = MetaData.MetaData(read(file_path)).data
        raw_data["filepath"] = file_path
        documents.append(raw_data)
    return documents
