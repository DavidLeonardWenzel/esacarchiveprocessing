import copy
import src.control.misc as misc


class JsonLDCreator:
    """
    This class takes a raw meta data entity as input.
    Output: JSON-LD formatted metadata-table
    Usage: Instantiate a jsonLDCreator-Object with the dict. from the toml-entity
    call "make" and put the resulting  into the <head> of your HTML and the google crawler will be able to read it
    """

    def __init__(self, data, warnings, science_branch):
        self.raw_data = copy.deepcopy(data)
        self.jsonLDList = []
        self.warnings = warnings
        self.science_branch = science_branch
        # a list of attributes that should be included in the JSON-LD
        # by defining subsets, you can crawl data for a mission or dataset into one TOML
        # and then target various goals (this json-ld, html tables) containing diff. info
        self.attributes_wanted = []
        self.attributes_digested = 0
        # is set dynamically per category (psa, helio, astro)
        self.white_list = []

    def make(self):
        """quasi-main,
        calls all the req. functions in order"""

        if self.data_is_valid():
            self.open_header()
            self.white_list_wanted_attributes(self.science_branch)
            self.remove_non_white_listed_attributes()
            self.add_crawled_attributes()
            self.add_generated_attributes()
            self.close_header()
            string = self.join_data()

        print(
            "Successfully created metadata-table parsing {x} fields".format(x=self.attributes_digested))
        return string

    def add_generated_attributes(self):
        self.add_esa_stamp()
        self.add_esa_thumbnail()
        self.add_intended_audience()
        # self.add_license()

    def add_trailing_space_to(self, string, goal):
        while len(string) > goal:
            string = string+" "
        return string

    def data_is_valid(self):
        """
        some properties may be required, some are encouraged. 
        here, all constraints are checked
        """
        if len(self.jsonLDList) != 0:
            self.warnings.add(
                "json", "Data found before opening script", "severe")

        description_length = len(self.raw_data["description"])
        if description_length < 50:
            self.raw_data["description"] = self.add_trailing_space_to(
                self.raw_data["description"], 50)
            self.warnings.add(
                "json", "Description is too short, google specified a string length between 50 and 5000.", "normal")
        try:
            self.raw_data["name"]
        except KeyError:
            self.warnings.add(
                "json", "Name field not found, for dataset-search it's mandatory", "severe")
        try:
            self.raw_data["license"]
        except KeyError:
            self.warnings.add(
                "json", "License field not found but heavily recommended", "normal")
        # severe errors quit the program, so we can return True
        return True

    def open_header(self):
        self.jsonLDList.append('''<script type="application/ld+json">
{\n\t"@context": "http://schema.org/",\n\t"@type" :"Dataset",''')

    def close_header(self):
        self.jsonLDList.append('</script>')

    def add_crawled_attributes(self):
        """currently adds top level dict stuff"""
        for key in self.raw_data:
            entry = self.raw_data[key]
            if type(entry) == str:
                self.str_digest(key, entry)
            elif type(entry) == list:
                self.list_digest(key, entry)

    def str_digest(self, key, value):
        if key == "instrument":
            key = "variableMeasured"
        if key == "DOI":
            key == "@id"
        self.jsonLDList.append('\t"' + key + '": "' +
                               misc.clean_string(value, 2000)+'",')
        self.attributes_digested += 1

    # def add_license(self, value):
        #lic_annotation =   '''\t"license": {{"@type": "URL","url": "{x}"}},'''.format(x=value)
        # self.jsonLDList.append(lic_annotation)
        # self.attributes_digested+=1

    def list_digest(self, key, value):
        if len(value) != 0:
            list_entry = ""
            list_entry = list_entry + '\t"' + key + '": ['
            for sub_entry in value:
                assert type(sub_entry) == str
                list_entry = list_entry + '\n\t\t"'+str(sub_entry)+'",'
            # remove dangling comma:
            list_entry = list_entry[:-1]+"\n\t],"
            self.jsonLDList.append(list_entry)
            self.attributes_digested += 1

    def join_data(self):
        jsonLDString = "\n".join(self.jsonLDList)
        jsonLDString = self.remove_dangling_comma(jsonLDString)
        return jsonLDString

    def remove_dangling_comma(self, string, comma_index=-11):
        return string[:comma_index] + "\n}" + string[comma_index+1:]

    def white_list_wanted_attributes(self, science_branch):
        planetary = [
            "name",
            "mission"
            "url",
            "abstract",
            "datePublished",
            "DOI",  # to appear as @id, generated after page creation
            "keywords",  # are generated in preprocessing
            "instrument",  # to appear as measurementTechnique
            "publisherAndRegistrant",  # gets added with add_esa_stamp()
            "version",
            "thumbnailURL",  # gets added through add_esa_thumbnail()
            "audience",  # gets added through add_intended_audience()
            "temporalCoverage",
            "description",
            "license"  # gets added through add_licensing_info(), currently unused
        ]
        heliophysics = [
            "publication",
            "name",
            "mission"
            "url",
            "abstract",
            "datePublished",
            "DOI",  # to appear as @id, generated after page creation
            "keywords",  # are generated in preprocessing
            "instrument",  # to appear as measurementTechnique
            "publisherAndRegistrant",  # gets added with add_esa_stamp()
            "version",
            "thumbnailURL",  # gets added through add_esa_thumbnail()
            "audience",  # gets added through add_intended_audience()
            "temporalCoverage",
            "description",
            "license"  # gets added through add_licensing_info(), currently unused
        ]
        astronomy = [
            "name",
            "mission"
            "url",
            "abstract",
            "datePublished",
            "DOI",  # to appear as @id, generated after page creation
            "keywords",  # are generated in preprocessing
            "instrument",  # to appear as measurementTechnique
            "publisherAndRegistrant",  # gets added with add_esa_stamp()
            "version",
            "thumbnailURL",  # gets added through add_esa_thumbnail()
            "audience",  # gets added through add_intended_audience()
            "temporalCoverage",
            "description",
            "license"  # gets added through add_licensing_info(), currently unused
        ]
        others = []

        if science_branch == "planetary":
            self.white_list.extend(planetary)
        elif science_branch == "heliophysics":
            self.white_list.extend(heliophysics)
            # todo
        elif science_branch == "astronomy":
            self.white_list.extend(astronomy)
            # todo
        elif science_branch == "others":
            self.white_list.extend(others)
            # todo

    def add_esa_thumbnail(self):
        self.jsonLDList.append(
            '"thumbnailUrl": "http://www.esa.int/identity/01L_color_03.html",')

    def add_esa_stamp(self):
        # optionally, we could appear as "author" or "creator" with the same format.
        stamp = '''\t"publisher  ":{\n\t\t"@type": "Organization",
        "url": "http://www.esa.int/ESA",
        "name": "European Space Agency"\n\t},'''
        self.jsonLDList.append(stamp)

    def add_intended_audience(self):
        audience = '''\t"audience":{\n\t\t"@type": "Audience",
        "audienceType": ["Astronomers", "Space Community", "Data Scientists", "Machine Learning Users"]\n\t},'''

        self.jsonLDList.append(audience)

    def add_licensing_info(self, license):
        return '"license: "'+license+'"'

    def remove_non_white_listed_attributes(self):
        # if you want to see the warnings (in this case, see the removed attributes), create files with inLoggingMode=true() and look at the resulting log files
        kill_me = []  # list of keys to remove
        for key in self.raw_data:
            if key not in self.white_list:
                kill_me.append(key)

        for non_white_list_property in kill_me:
            self.warnings.add(
                "json",
                "Unexpected attribute {x} will be removed!".format(
                    x=non_white_list_property),
                "normal")
            self.raw_data.pop(non_white_list_property)
