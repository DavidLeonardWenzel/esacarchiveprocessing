import os

import src.boundary.read_write as io
import src.control.HTMLTableCreator as HTMLTableCreator
import src.control.HTMLWrapper as HTMLWrapper
import src.control.JsonLDCreator as JsonLDCreator
import src.entity.ExpChecklist as ExpChecklist
import src.control.Spider as Spider
import re


def is_link(link):
    pattern = r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"
    return re.match(pattern, link)


def create_html(raw_data, warnings, mission_info):
    script = create_json(raw_data, warnings, mission_info["science_branch"])
    html_table = create_html_table(raw_data, warnings)

    # CREATE VALID HTML, COMBINING SCRIPT AND TABLE:
    htmlWrapper = HTMLWrapper.HTMLWrapper(
        raw_data, script, html_table, warnings, mission_info)
    return htmlWrapper.make()


def create_json(raw_data, warnings, science_branch):
    jsonLDCreator = JsonLDCreator.JsonLDCreator(
        raw_data, warnings, science_branch)  # give data to the creator
    return jsonLDCreator.make()


def create_html_table(raw_data, warnings):
    """returns an html table containing the attributes in raw_data"""
    htmlTableCreator = HTMLTableCreator.HTMLTableCreator(
        raw_data, warnings, strict_mode=True)
    return htmlTableCreator.make()


def sure_to_overwrite(mission_name):
    status = bool
    if file_exist("resources/checklists/"+mission_name):
        inp = input(
            "Checklist already exists! Overwrite and start over? [y/N]")
        if inp.lower() == "y":
            status = True
        else:
            status = False
    else:
        status = True
    return status


def ensure_write_safety(mypath):
    if not path_exist(mypath):
        make_path(mypath)


def peek(mission_name):
    """looks for a checklist with the given mission name and prints all found missions/instruments/experiment names and crawl status"""
    expChecklist = ExpChecklist.ExpChecklist(mission_name)
    expChecklist.load()
    expChecklist.lengthyprint()


def has_unclean_experiments_left(mission_name):
    expChecklist = ExpChecklist.ExpChecklist(mission_name)
    expChecklist.load()
    flags = []
    for instr in expChecklist.collection:
        for exp in expChecklist.collection[instr]:
            flags.append(not exp.crawled)
    if sum(flags) == 0:
        return False
    else:
        print("There are", sum(flags), "unclean experiments left.")
        return True


def file_exist(mypath):
    return os.path.isfile(mypath)


def path_exist(mypath):
    return os.path.isdir(mypath)


def make_path(mypath):
    os.mkdir(mypath)


def clean_string(string, limit):
    return clean(truncate_to_limit(string, limit))


def clean(text):
    return text.replace("=", "").replace("-", "")


def truncate_to_limit(string, limit=2000):
    if len(string) <= limit:
        return string
    else:
        return string[:1950]+"[truncated, see actual data for full text]"


def truncate_to_url(string, url, limit=2000):
    if len(string) <= limit:
        return string
    else:
        return string[:1950]+'<a href = "'+url+'"> [read more]</a>"'


def create_checklist(mission):
    """creates a checklist object, which is a list-like object containing experiment names
    this checklist is used to flag progress of the actual crawl process"""
    if sure_to_overwrite(mission["name"]):
        expChecklist = ExpChecklist.ExpChecklist(mission["name"])
        spider = Spider.Spider(mission)
        expChecklist = spider.populateExpChecklist(expChecklist)
        expChecklist.save()
        print("created checklist for", mission["name"])
    else:
        print("skipping checklist creation for", mission["name"])


def truncate_to_read_more(string, limit=2000):
    if len(string) <= limit:
        return string
    else:
        return string[:1950]+'''<span id="dots">...</span><span id="more">'''+string[1950:] + '''</span><button onclick="myFunction()" id="myBtn">Read more</button>'''


def regex_remove(pattern, string):
    return re.sub(pattern, "", string)


def crawl_type_is_supported(name, crawl_format):
    """checks if the mission can be handled. supported missions are listed in the class attributes"""
    known = False
    if crawl_format == "pds3":
        known = True
    elif crawl_format == "manual":
        known = True
    elif crawl_format == "api_v1":
        known = True
    else:
        print(f"""Can't crawl {name}, the format {crawl_format} is not supported. 
        Supported formats are: pds3, manual, api_v1 (cluster machine interface)
        """)
    return known


def html_readable_rawtext(words, max_word_count, br_tag=True):
    words = words.split()
    new_text = ""
    seperator = "\n"
    if br_tag:
        seperator = "<br>"
    word_count = 0
    for word in words:
        new_text += word + " "
        word_count += 1
        if word_count == max_word_count or "." in word:
            new_text += seperator
            word_count = 0
    return new_text
