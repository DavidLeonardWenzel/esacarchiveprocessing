class HTMLWrapper:
    """
    Super simple HTML wrapper that you can use to link the JSON-LD to the table. 
    Someone who actually does front-end should delete this class, take the two arguments 
    and build an actually good website. for now, this works though. 
    Usage: instantiate an HTMLWrapper object by providing 2 strings: 
    1. one containing the JSON-LD markup table
    2. the other containing the HTML body, basically only the table
    """

    def __init__(self, raw_data, head_data, table_data, warnings, mission_info):
        self.raw_data = raw_data
        self.head_data = head_data
        self.table_data = table_data
        self.warnings = warnings
        self.mission_info = mission_info

    def make(self):
        return """<!DOCTYPE HTML>
<HTML lang="en">     
    <HEAD>
        <style>
        #more {display: none;}
        </style>

        <META CHARSET="UTF-8">
        <META NAME ="author" content="David L. Wenzel">
        <TITLE>Dataset provided by the European Space Agency</TITLE>
        <link rel="stylesheet" href="../../../css/style.css">""" + self.head_data+"""
    </HEAD>
    <BODY> 
    <h1>
    A dataset provided by the European Space Agency
    </h1>
    <div style="text-align: right"><img src="../../../img/esa_logo_silver.svg" alt="Logo of the European Space Agency"/></div>
    <br/>"""+self.add_logo()+"""<br/>
    """+self.table_data+"""

    <script>

function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
  }
}
</script>

    </BODY>
</HTML>"""

    def add_logo(self):
        img_path = "../../../img/"
        ending = "_CMYK.png"
        mission = self.mission_info["name"]
        if mission == "":
            self.warnings.add(
                "wrapper", "No mission field and therefore no logo was found for the mission", "normal")
            return ""
        else:
            return '<img src="{x}" alt="Logo of the Mission", class="center"/>'.format(x=img_path+mission+ending)
