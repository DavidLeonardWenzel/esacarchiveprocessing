import src.boundary.read_write as io


class CollectionCreator:
    def __init__(self, mission_info):
        self.mission_info = mission_info
        self.science_branch = mission_info["science_branch"]
        self.name = mission_info["name"]
        self.base_path = "resources/toml/"+self.science_branch+"/"+self.name+"/"
        self.raw_data = None
        self.get_raw_data()
        self.wanted_csv_fields = [
            "url", "name", "mission"
        ]

    def get_raw_data(self):
        all_file_paths = io.get_all_file_paths(self.base_path)
        self.raw_data = io.get_all_docs(all_file_paths)

    def create(self):
        htmls = []
        csv_rows = [["DOI", "Landing Page URL", "Satellite ID", "Instrument Code(s)", "Product type(s)", "Data Collection Name", "Data Collection Version", "Data Set Short Name", "Registrant",
                     "Department", "Contributors", "Collection Creation Year", "Collection Publication Year", "Collection Description", "Collection File Format", "Citation Guidelines for Landing Page"]]
        for doc in self.raw_data:
            htmls.append([doc["landingpage"], doc["name"]])
            csv_rows.append(self.get_csv(doc))
        html = self.wrap_htmls(htmls)

        # transform list of comma seperated values into a csv string to write:
        csv_string = ""
        for row in csv_rows:
            row_as_string = ",".join(row)
            csv_string = csv_string + row_as_string + "\n"
        return html, csv_string

    def wrap_htmls(self, htmls):
        """wraps a list of landingpages in html code"""
        return """<!DOCTYPE HTML>
<HTML lang="en">
    <HEAD>
        <META CHARSET="UTF-8">
        <META NAME ="author" content="David L. Wenzel">
     </HEAD>
    <BODY>
    """+self.ul(htmls)+"""
    </BODY>
</HTML>"""

    def ul(self, elements):
        html_list = "<ul>\n"
        for s in elements:
            html_list = html_list + "\t<li>" + \
                self.wrap_as_link(s[0], s[1]) + "</li>\n"
        html_list = html_list + "</ul>"
        return html_list

    def wrap_as_link(self, url, visible_name):
        return '<a href="'+url+'">'+visible_name+'</a>'

    def get_csv(self, experiment):
        """we create a csv row with 16 fields in accordance to the doi template seen in dev/ESA - DOI Template.xlsx"""
        registrant = "European Space Agency D/SCI"
        version = ""
        datePublished = ""
        department = "D/SCI"
        contributors = "European Space Agency"
        try:
            version = experiment["version"]
            datePublished = str(experiment["datePublished"])
        except:
            pass
        csv_row = ["", experiment["landingpage"], "",
                   "", "", experiment["name"].replace(",", ""), version, "", registrant, department, contributors, "", datePublished, experiment["description"].replace(",", "").replace("\n", ""), "", ""]
        return csv_row
