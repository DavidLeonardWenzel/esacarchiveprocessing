# use requests and pvl to find all metadata found in the psa archive!
from ftptool import FTPHost
import pvl
import src.entity.Experiment as Experiment
import src.entity.ExpChecklist as ExpChecklist
import src.boundary.read_write as io
import src.control.api_request_interface as api_request_interface
import src.control.misc as misc
import toml


class Spider():
    """Used to crawl & collect mission directory urls and parse pvl documents.
    use populateExpChecklist() to create all experiments
    use parse_cat() to get a dictionary-like object from pds data"""

    def __init__(self, mission_info):
        self.url = "npsa01.esac.esa.int"
        self.esac_ftp = None

        self.mission_info = mission_info
        self.crawl_format = mission_info["format"]
        self.mission_name = mission_info["name"]
        self.science_branch = mission_info["science_branch"]
        # todo check where mission and where mission name is used/needed
        self.mission_type = None
        self.mission_dir = None

    def init(self):
        """collects everything for that mission"""
        if self.crawl_format == "pds3":
            misc.create_checklist(self.mission_info)
            self.pds3()
            if misc.has_unclean_experiments_left(self.mission_name):
                print(
                    f"can't parse enough information to create sites for all things regarding {self.mission_name}")
                print(
                    "If you fix this and re-run without overwriting the checklist, I will only recrawl the failed ones!!")
        elif self.crawl_format == "api_v1":
            self.api_v1()
        elif self.crawl_format == "api_v2":
            self.api_v2()

    def api_v1(self):
        # collect
        collections = api_request_interface.init_helio(self.mission_info)
        # write
        for collection in collections:
            path = "resources/toml/"+self.science_branch + "/" + self.mission_name + "/"
            misc.ensure_write_safety(path)
            toml_string = toml.dumps(collection)
            io.write(toml_string, path+collection["name"]+".toml")

    def api_v2(self):
        # TODO
        # collect:
        collections = api_request_interface.init_astro(self.mission_info)
        # write:
        for collection in collections:
            path = "resources/toml/"+self.science_branch + "/" + self.mission_name + "/"
            misc.ensure_write_safety(path)
            toml_string = toml.dumps(collection)
            io.write(toml_string, path+collection["name"]+".toml")

    def pds3(self):
        self.connect()
        expChecklist = ExpChecklist.ExpChecklist(self.mission_name)
        expChecklist.load()
        exps = expChecklist.collection
        # upped is a copy of the checklist.  after an exp reaches clean crawled+written status
        # that exp gets into the upped version (replacing the old)
        # no matter what goes wrong, the whole upped checklist replaces the old checklist
        # thus, even if some exp ran into trouble or connection resets, those affected experiments would have clean_crawl=False flag
        # after fixing the issue and rerunning, only clean_crawl=False are crawled, leaving the finished stuff unaffected!
        expChecklistUpped = ExpChecklist.ExpChecklist(
            self.mission_name)
        expChecklistUpped.load()
        expsUpped = expChecklistUpped.collection

        for instrument in exps:
            print("at instrument", instrument)
            for i, experiment in enumerate(exps[instrument]):
                if experiment.crawled == False:
                    try:
                        experiment.instrument_name = instrument
                        experiment.init(self, self.science_branch)
                        expsUpped[instrument][i] = experiment
                    except:
                        print("ran into exception, gotta rerun some day")
                        self.close()
                        self.connect()
                else:
                    print("already crawled", experiment.name)

        expChecklistUpped.save()
        print("done")
        self.close()

    def set_mission_dir(self):
        self.mission_dir = "/pub/mirror/" + self.mission_name

    def connect(self):
        self.esac_ftp = FTPHost.connect(
            self.url, user="anonymous", password="py3.7@")

    def close(self):
        self.esac_ftp.quit()

    def quit_connection(self):
        self.esac_ftp.quit()

    def get_file(self, directory):
        return self.esac_ftp.file_proxy(directory).download_to_str()

    def get_dirs(self, directory):
        dirs, _files = self.esac_ftp.listdir(directory)
        subdirs = {}
        for folder in dirs:
            subdirs[folder] = directory+"/"+folder
        return subdirs

    def get_file_dirs(self, directory):
        _dirs, files = self.esac_ftp.listdir(directory)
        file_collection = {}
        for file in files:
            file_collection[file] = directory+"/"+file
        return file_collection

    def parse_cat(self, cat_string):
        res = ""
        try:
            res = pvl.loads(cat_string)
        except:
            cat_string = cat_string.replace("\\", "/")
            res = pvl.loads(cat_string)
        finally:
            if res == "":
                res = self.parse_as_windows(cat_string)
            return res

    def parse_as_windows(self, cat_string):
        res = ""
        try:
            cat_string = cat_string.decode('windows-1252')
            cat_string = cat_string.replace("\\", "/")
            res = pvl.loads(cat_string)
        finally:
            return res

    def create_unique_experiments(self, experiment_dirs):
        # 1. find out all unique experiment names
        unique_exps = set()
        for exp in experiment_dirs:
            unique_exps.add(exp[:-5])

    # 2. create experiment objects:
        experiments = []
        for unique_exp in unique_exps:
            experiments.append(Experiment.Experiment(
                unique_exp, self.science_branch, self.mission_name))

    # 3. populate the experiments with path and version number s
        for expKey in experiment_dirs:
            name = expKey[:-5]
            version = expKey[-5:]
            path = experiment_dirs[expKey]

            for exp in experiments:
                if exp.name == name:
                    exp.versions.append(version)
                    exp.paths.append(path)
        return experiments

    def populateExpChecklist(self, expChecklist):
        """this creates a NEW checklist with all experiments the crawler can find"""

        self.connect()
        self.set_mission_dir()
        expChecklist.collection = self.get_dirs(self.mission_dir)
        for instrument_subdir in expChecklist.collection:
            exps = self.get_dirs(self.mission_dir+"/"+instrument_subdir)
            unique_exps = self.create_unique_experiments(exps)
            expChecklist.collection[instrument_subdir] = unique_exps
        return expChecklist
