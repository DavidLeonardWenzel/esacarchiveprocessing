import requests
import time
import json


def init_helio(mission_info):
    fields = "SELECTED_FIELDS=EXPERIMENT.NAME,EXPERIMENT.LONG_NAME"
    url = build_name_url(mission_info, fields)
    instrument_names = get_names(json_to_dict(get_resp(url), ""))
    print("trying to fetch\n", url)
    print("response is", instrument_names)
    collections = []
    attribute_fields = "SELECTED_FIELDS=INSTRUMENT.DESCRIPTION,MISSION.START_DATE,MISSION.END_DATE,MISSION.DESCRIPTION,DATASET.DATASET_ID,DATASET.TITLE,DATASET.DESCRIPTION"
    for short_name, long_name in instrument_names:
        print("now gathering data for", short_name)
        info = {}
        info["mission"] = mission_info["name"]
        info["name"] = long_name.replace("/", "-")
        info["url"] = mission_info["url"]
        info["creatorContact"] = mission_info["creatorContact"]
        fetched_info = json_to_dict(get_resp(build_attribute_url(
            mission_info, attribute_fields, short_name)), short_name)
        # per collection we have a number of experiments from which we build one decsrition and one temporal coverage
        if fetched_info["data"] == []:
            info["description"] = "n/a"
            info["temporalCoverage"] = "n/a"
        else:
            desc = fetched_info["data"][0]["INSTRUMENT.DESCRIPTION"]
            desc = desc + "\nThis collection contains the following experiments:\n"
            temporalCoverage = fetched_info["data"][0]["MISSION.START_DATE"] + \
                " - " + fetched_info["data"][0]["MISSION.END_DATE"]
            for exp in fetched_info["data"]:
                # print(exp)
                exp_info = exp["DATASET.DATASET_ID"]+", " + \
                    exp["DATASET.TITLE"]+"\n"+exp["DATASET.DESCRIPTION"]
                desc = desc+exp_info
            info["description"] = desc
            info["temporalCoverage"] = temporalCoverage
        collections.append(info)
    return collections


def build_attribute_url(mission_info, attributes, instrument):
    mission = mission_info["name"]
    return f"https://csa.esac.esa.int/csa/aio/metadata-action?{attributes}&RESOURCE_CLASS=EXPERIMENT&EXPERIMENT.NAME={instrument}&MISSION.NAME={mission}&RETURN_TYPE=JSON"


def build_name_url(mission_info, fields):
    mission = mission_info["name"]
    return f"https://csa.esac.esa.int/csa/aio/metadata-action?{fields}&MISSION.NAME={mission}&RESOURCE_CLASS=EXPERIMENT&RETURN_TYPE=JSON"


def get_resp(url, tries=0):
    req = requests.get(url)
    if req.status_code == 200:
        return req.text
    else:
        if tries != 3:
            print("no response from server, retrying in 5")
            time.sleep(5)
            get_resp(url, tries+1)
        else:
            print("no response, quitting")
            quit()


def json_to_dict(json_string, instrument_name):
    try:
        return json.loads(json_string.replace('\n', ' '), strict=False)
    except:
        print("warning, couldnt load json at instr", instrument_name)
        return json_string


def get_names(response):
    names = []  # list of form (short name, long name)

    for field in response["data"]:
        shorty = field["EXPERIMENT.NAME"]
        longy = field["EXPERIMENT.LONG_NAME"]
        names.append((shorty, longy))

    return names


def init_astro(mission_info):
    collections = []
    urls = get_predefined_urls()
    req = requests.get(urls["name_title_abstract"])
    probs_and_obs = json.loads(req.text)
    last_proposal = 0
    # remove :5 and go over all once everything works
    for i, prob_and_ob in enumerate(probs_and_obs["data"][:5]):
        print(i)
        get_pi_and_abstract(last_proposal, prob_and_ob)
        url = "http://nxsa.esac.esa.int/tap-server/tap/sync?REQUEST=doQuery&LANG=ADQL&FORMAT=json&QUERY=SELECT+observation_oid,pps_version,proprietary_end_date,start_utc,end_utc+FROM+v_public_observations+WHERE+observation_id='" + \
            prob_and_ob[1]+"'"
        req = requests.get(url)
        obs_data = json.loads(req.text)["data"]
        print(obs_data)  # pps version: per proposal, collect all, then find lowest comparing for first 4 digits and then take the whole number as the        version, for start and end utc , find lowest across all start times and highest across all end times and use that for temporal coverage

        # prop end date should be the most recent date per proposal
        oid_url = "http://nxsa.esac.esa.int/tap-server/tap/sync?REQUEST=doQuery&LANG=ADQL&FORMAT=json&QUERY=SELECT+instrument+FROM+v_exposure+WHERE+observation_oid=" + \
            obs_data[0][0]
        req = requests.get(oid_url)
        instruments = json.loads(req.text)["data"]
        print(instruments)  # set per proposal

        last_proposal = prob_and_ob[0]
    return collections


def get_pi_and_abstract(last_proposal, prob_and_ob):
    if last_proposal != prob_and_ob[0]:
        url = "http://nxsa.esac.esa.int/tap-server/tap/sync?REQUEST=doQuery&LANG=ADQL&FORMAT=json&QUERY=SELECT+pi_title,pi_first_name,pi_surname,title,abstract+FROM+v_proposal+WHERE+proposal_oid=" + \
            prob_and_ob[0]
        req = requests.get(url)
        pi_and_abstract = json.loads(req.text)
        print(pi_and_abstract["data"])


def get_predefined_urls():
    urls = {}
    urls[
        "example"] = "http://nxsa.esac.esa.int/tap-server/tap/sync?REQUEST=doQuery&LANG=ADQL&FORMAT=json&QUERY=SELECT+top+10+ra,dec,observation_id,revolution+FROM+v_public_observations+WHERE+1=intersects(observation_fov_scircle,circle('ICRS',10.3,41.5,0.1))+AND+revolution>1000"
    urls["name_title_abstract"] = "http://nxsa.esac.esa.int/tap-server/tap/sync?REQUEST=doQuery&LANG=ADQL&FORMAT=json&QUERY=SELECT+proposal_oid,pi_title,pi_first_name,pi_surname,title,abstract+FROM+v_proposal"
    return urls
