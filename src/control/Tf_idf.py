import math
import re

import numpy as np
import toml

import src.boundary.read_write as io
import src.entity.MetaData as MetaData


class Tf_idf:
    def __init__(self, mission_info):
        self.science_branch = mission_info["science_branch"]
        self.name = mission_info["name"]
        self.base_path = "resources/toml/"+self.science_branch+"/"+self.name+"/"
        self.character_re = re.compile('[a-zA-Z]')

    def init(self):
        self.create_weights()

    def create_weights(self):
        all_file_paths = io.get_all_file_paths(self.base_path)
        if not self.keywords_already_created(all_file_paths[0]):
            print("starting tf-idf-process")
            self.raw_data = io.get_all_docs(all_file_paths)
            documents = self.get_preprocessed_textual_info(self.raw_data)
            word_lookup = self.get_word_lookup(documents)
            # swap keys and values
            self.dictionary = {v: k for k, v in word_lookup.items()}
            # now we can look up words with their corresponding numbers in our vocabulary
            idf_weights = self.calc_idf(documents)

            self.tf_idf_representations = [self.calc_tf_idf(
                self.get_vec_repr(doc, word_lookup),
                idf_weights)
                for doc in documents]
            self.write_keywords_to_files()
        else:
            print("keywords already exist, skipping tf idf process")

    def keywords_already_created(self, example_path):
        raw = io.read_toml(example_path)
        keywords = raw.get("keywords", "")
        if keywords == "":
            return False
        else:
            return True

    def write_keywords_to_files(self):
        for i, tf_idf_rep in enumerate(self.tf_idf_representations):
            top_words = self.get_top_x_words(tf_idf_rep, 5)
            self.raw_data[i]["keywords"] = top_words
            io.write(toml.dumps(self.raw_data[i]),
                     self.raw_data[i]["filepath"])

    def get_preprocessed_textual_info(self, raw_data):
        prepped_docs = []
        for doc in raw_data:
            abstract = doc.get("abstract", "")
            desc = doc.get("description", "")
            prepped_docs.append(self.prep(abstract+" "+desc))
        return prepped_docs

    def prep(self, doc):
        return list(filter(lambda x: re.search(self.character_re, x.replace("T", "")), doc.split()))

    def calc_idf(self, documents):
        '''calcs log of doc. freq for each word in the vocabulary'''
        n = len(documents)
        terms = {}
        for document in documents:
            # set because we only want to count up once if the word exists, no matter how often
            unique_words = set(document)
            for word in unique_words:
                terms[word] = terms.get(word, 0) + 1
        for each_term in terms:
            terms[each_term] = math.log(n/terms[each_term])
        return(terms)

    def calc_tf_idf(self, document, idf_weights):
        """calcs the tf-idf weights, takes a vectorized document"""
        for index, tf in enumerate(document):
            if tf != 0:
                # now replace tf with real valued tf-idf value:
                document[index] = (1 + math.log(tf)) * \
                    idf_weights[self.dictionary[index]]
        return(np.array(document))
        # document now looks like so: [0, 0, 0, 0, 7.455563363104405, 0, 0 . . .]

    def get_word_lookup(self, documents):
        """this function creates a dict that can be used as a lookup for
        sparse vector representation of documents.
        for the whole vocabulary (all documents) it maps a number to each word"""

        unique_words = set()
        for doc in documents:
            unique_words = unique_words | set(doc)

        vec = {}
        for num, word in enumerate(unique_words):
            vec[word] = num
        return vec

    def get_vec_repr(self, document, word_lookup):
        vec = [0] * len(word_lookup)  # initialize a vector the size of vocab
        for word in document:
            vec[word_lookup[word]] += 1
        # now looks like this:
        # [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 0] ... (mapping term frequencies on doc level)
        return vec

    def get_top_x_indices(self, arr, x):
        """takes an arr and an integer x, returns x indices, corresponding to the x highest values in the array"""
        return arr.argsort()[-x:][::-1]

    def get_top_x_words(self, doc_as_tfidf_vec, x):
        top_words = []
        heaviest_i = self.get_top_x_indices(doc_as_tfidf_vec, 5)
        top_words = []
        for heavy_i in heaviest_i:
            word = self.dictionary[heavy_i]
            top_words.append(word)
        return top_words
