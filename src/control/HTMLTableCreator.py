import pandas as pd
import copy
import src.control.misc as misc


class HTMLTableCreator:
    """
    creates the title, table, and logo html code.
    expects the template data as a dict as provided by the toml metadata entity.
    if you set strict_mode to True, only white-listed attributes will be included.
    thus, you can add as much data as you want to the template, only a
    subset will be included in the resulting table and therefore on the html page
    """

    def __init__(self, raw_data, warnings, strict_mode=True):
        self.raw_data = copy.deepcopy(raw_data)
        self.warnings = warnings
        self.bodyList = []
        self.strict_mode = strict_mode
        self.white_list = [
            "publication",
            "name",
            "url",
            "datePublished",
            "DOI",
            "publisherAndRegistrant",  # gets added through add_publisher_and_registrant()
            "version",
            "satelliteId",  # gets added through add_satellite_id()
            "temporalCoverage",
            "abstract",
            "mission",
            "missionDescription",
            "description",
            "instrument",
            "creatorContact",
            "creditGuidelines"  # gets added through add_credit_guidelines()
        ]

    def make(self):
        """
        quasi-main, calls all functions in correct order
        """
        self.add_publisher_and_registrant()
        # self.add_satellite_id()
        self.add_credit_guidelines()
        if self.strict_mode:
            self.remove_non_white_listed_attributes()
        self.make_fields_human_readable()
        self.upper_case_all_keys()
        # self.add_dummy_doi()  # todo: exchange with actual DOI
        self.add_table()
        body = self.join_data()
        print("Created HTML body containing the meta data")
        return body

    def make_fields_human_readable(self):
        """
        converts raw string links to html links,
        replaces properties written in 'camelCase' to 'camel case'
        removes python list syntax to comma-seperated values
        """
        replace_after_iteration = []
        # MAKE URL VALUE TO LINK:
        as_link = self.make_html_link(self.raw_data["url"])
        self.raw_data["url"] = as_link

        if misc.is_link(self.raw_data["creatorContact"]):
            as_link = self.make_html_link(self.raw_data["creatorContact"])
            self.raw_data["creatorContact"] = as_link

        for key in self.raw_data:
            # TRUNCATE TO 2000
            if type(self.raw_data[key]) == str:

                self.raw_data[key] = misc.truncate_to_read_more(
                    self.raw_data[key])

                self.raw_data[key] = misc.regex_remove(
                    "==", self.raw_data[key])
                self.raw_data[key] = self.raw_data[key].replace("\n", "<br>")
                #self.raw_data[key] = self.raw_data[key].replace("\t", "&nbsp")
            # MAKE LISTS COMMA-SEPERATED FOR HUMANS:
            if type(self.raw_data[key]) == list:
                list_as_string = ", ".join(self.raw_data[key])
                self.raw_data[key] = list_as_string

            # ADD SPACES TO CAMEL CASE FOR HUMANS:
            if self.is_camel_case(key):
                human_readable_form = self.camel_case2human_readable(key)
                replace_after_iteration.append(
                    (key, human_readable_form, self.raw_data[key]))

        for key, human_readable_form, value in replace_after_iteration:
            self.raw_data[human_readable_form] = value
            self.raw_data.pop(key)

    def add_dummy_doi(self):
        self.raw_data["DOI"] = "10.5270/esa-[xxxxxxx] (this is a placeholder!)"

    def add_satellite_id(self):
        # try:
        #    mission = self.raw_data["mission"]
        #    if mission == "EARTH":
        #        self.raw_data["satelliteID"] = mission
        #    else:
        self.raw_data["satelliteId"] = "ESA"

    def add_publisher_and_registrant(self):
        self.raw_data["publisherAndRegistrant"] = '<a href = "http://www.esa.int/ESA"> European Space Agency </a>'

    def add_credit_guidelines(self):
        self.raw_data["creditGuidelines"] = "When publishing any works related to this experiment, please cite the DOI found herein."

    def is_camel_case(self, string):
        """helper func. that returns true if the given string is in camel keys
        but NOT if it is all caps or contains spaces already
        """
        if " " in string:
            return False
        elif string.isupper():  # all caps suggests the string is an acronym
            return False
        else:
            for cha in string[1:]:
                if cha.isupper():
                    return True
            return False

    def camel_case2human_readable(self, string):
        indexes = self.find_indexes(string)
        readable = self.add_spaces(string.lower(), indexes)
        return readable

    def find_indexes(self, string):
        indexes = []
        for i, cha in enumerate(string):
            if cha.isupper():
                indexes.append(i)
        return indexes

    def add_spaces(self, string, indexes):
        list_string = [char for char in string]
        already_added = 0
        for upper_index in indexes:
            list_string.insert(upper_index+already_added, " ")
            already_added += 1
        return "".join(list_string)

    def make_html_link(self, string):
        """expects a string and returns the string embedded into html markup
        so that it's an actual link"""
        return '<a href="' + string + '">'+string + '</a>'

    def remove_non_white_listed_attributes(self):
        # if you want to see the warnings (in this case, see the removed attributes), create files with inLoggingMode=true() and look at the resulting log files
        kill_me = []  # list of keys to remove
        for key in self.raw_data:
            if key not in self.white_list:
                kill_me.append(key)
        for non_white_list_property in kill_me:
            self.warnings.add(
                "table",
                "Unexpected attribute {x} will be removed!".format(
                    x=non_white_list_property),
                "normal")
            self.raw_data.pop(non_white_list_property)

    def add_table(self):
        sorted_df = self.sort_fields_as_df(self.raw_data)
        table = sorted_df.to_html(
            classes="table", header=False, escape=False, index=False)
        # to_html() includes obsolete table styling, removing:
        table = table.replace('border="1" class="dataframe table"', '')
        self.bodyList.append(table)

    def join_data(self):
        return "\n</br>".join(self.bodyList)

    def upper_case_all_keys(self):
        updated_keys = []
        for key in self.raw_data:
            updated_keys.append((key, self.get_upper(key)))
        for old_key, new_key in updated_keys:
            self.raw_data[new_key] = self.raw_data[old_key]
            self.raw_data.pop(old_key)

    def get_upper(self, string):
        if string == "url":
            return "URL"
        else:
            return " ".join([word[0].upper()+word[1:] for word in string.split()])

    def sort_fields_as_df(self, raw_data):
        # todo this should be an entity and NOT be dependent on the writing after the corrections..
        order = {'Name': 1,
                 'DOI': 2,
                 'Mission': 3,
                 'URL': 4,
                 'Abstract': 5,
                 'Description': 6,
                 'Publication': 7,
                 'Instrument': 8,
                 'Temporal Coverage': 9,
                 'Version': 10,
                 'Mission Description': 11,
                 'Creator Contact': 12,
                 'Date Published': 13,
                 'Publisher And Registrant': 14,
                 'Credit Guidelines': 15}
        # satelliteId ?
        list_key_value = [[k, v] for k, v in raw_data.items()]
        existing_fields = []
        for i, key_value in enumerate(list_key_value):
            wanted_index = order.get(key_value[0], None)
            if wanted_index:
                existing_fields.append([key_value[0], i, wanted_index])
        # the wanted index is at index 2
        Sorted = self.sublist_sort(existing_fields, 2)
        for field in Sorted:
            field[1] = raw_data[field[0]]
            field.pop()
        df = pd.DataFrame(Sorted)
        return df

    def sublist_sort(self, sub_list, index):
        sub_list.sort(key=lambda x: x[index])
        return sub_list
