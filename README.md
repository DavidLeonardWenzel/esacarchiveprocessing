# ESA crawler

In 2019/2020, I worked for the European Space Agency (ESA) for 6 month. The traineeship resulted in [this paper](https://doi.org/10.1016/j.asr.2021.01.035), published in Advances in Space Research. All scripts for this work are collected in this repository. 


## Objectives
1. ) gather all metadata found in the [ESAC Archive data](https://www.cosmos.esa.int/web/esdc) into easy-to-process .toml-files
2. ) transform all the data into a webpage which serves as:  
    - a page that can be used as a DOI landing page to cite the dataset
    - a page understood and indexed by [Google Dataset Search](https://toolbox.google.com/datasetsearch). To that end, all content in the .toml from step 1 that is suitable will be transformed into the [JSON-LD format](https://json-ld.org/), annotated in [schema-org format](https://schema.org/Dataset).
        - for more information about how indexing with Google works, see 
            - [here](https://support.datacite.org/docs/how-do-i-expose-my-datasets-to-google-dataset-search) 
            - [or here](https://www.blog.google/products/search/making-it-easier-discover-datasets/)
3. ) transform the data into a .csv-file that can be used to register a DOI in accordance with ESRINs guidelines  
---
## Usage
The following is a guideline on how to use this collection of scripts.  
The project is split into three subparts: Astronomy, Planetary and Heliophysics. Each has its own or multiple related crawling methods and requirements.  
**In general**, all missions get crawled individually, and you can inspect and edit their processing level in the file `resources/mission_info.toml`. There, all missions are listed. Per mission, you will find:  
- name: a string, the name to be used for the mission (used for folder creation and the field 'mission name' on the webpage)
- science branch: a string, indicating whether this mission belongs to "planetary", "heliophysics" or "astronomy" (please use only those 3 values)
- crawled: a boolean, when this is set to `false`, the main function will crawl, process and create the various needed files for the mission
- format: a string, indicating in which format the mission is stored, so that it can be crawled (`pds3`, `manual`, `api_v1` and `api_v2` are the only valid options)  

For each mission, the script will execute the following three functions:  
- `crawl()`: attempts to access the various servers to crawl the missions and writes .toml files containing all the gathered data
- `post_process_data()`: generates keywords for all the missions using [tf-idf weighting](https://en.wikipedia.org/wiki/Tf%E2%80%93idf). It furthermore creates index-pages (a list of all mission names in .html form) and writes them to `/resources/html/data/[branch]/`. Those webpages are used in in [cosmos](cosmos.esa.int/) for the DOI overview pages! Lastly, the function creates .csv files and adds them to `/resources/toml/[branch]/`. These .csv lists are needed by ESRIN for the registration of a DOI. 
- `folder_to_html()`: goes over the list of .toml files to create the .html landing pages 

## Adding missions
**If you wish to add a mission**, create another entry with the same schema! For example, if the juice mission gets added in pds3 format, you could simply add the following to mission_info.toml:  
``` 
[juice]
name = "JUICE"
science_branch = "planetary"
format = "pds3"
crawled = false 
```   
If you need to add a new crawling type (for e.g. `pds4`), you need to add the code instructing how to crawl it in `src.control.Spider` and add another elif to the `init` function. As long as that function writes a .toml file, the rest of the script will work. If you are unsure how the resulting .toml file needs to look, have a look at the template at `/resources/misc/template.toml`. For a human readable overview, have a look at `/resources/misc/collected_fields.txt`. Furthermore, there are many examples in `resources/toml/`. 

## Re-indexing
To re-index a mission and overwrite the existing checklists, .tomls and .htmls, simply change the boolean field "crawled" in `mission_info.toml` to `false`. Then, run `python main.py`. Warning: the process is very I/O intensive, creating several thousand small files. Some missions take a long time to re-index.  
## Adding new fields
If you add a field to the .toml field (either by manually writing it to the file or because future versions crawl different attributes), note that they will not be automatically included in either the JSON-LD or in the visible table for the landing pages. This is because **only white listed** attributes get written to the files. To add things to the white list of the JSON-LD part, add them to `src/control/JSONLDCreator.py` in the function `white_list_wanted_attributes()`. If you want to add them to the HTML-Table, add the keyword to the `white_list` attribute in the class `src/control/HTMLTableCreator.py` **and give them an number to order them by in `sort_fields_as_df()`**. The number describes at which row the field will appear. 

---
# Further Notes
## Planetary
Supported are **Giotto, Cassini Huygens, CHANDRAYAAN-1, Rosetta, Mars-Express** and **SMART-1**. The planetery branch is split into two crawling styles: pds3 format and pds4 format. Note that the pds4 format is currently not supported as the required metadata does not exist in the required granularity. *This means, the missions Bepi Colombo and Exo Mars are excluded*.  
If you re-run the script, you will get warnings for some experiments, notifying you that not enough metadata can be found. That is because the pds3-files documenting the missions are not homogenous. To fix a single experiments format and naming schema, you can add it in `src.entity.Experiment.py`, namely the functions `crawl_attributes()` and `set_dataset_attributes()` can be extended to look for specific file names and look for different keywords in those files. 
## Heliophysics
Supported are the missions **Cluster, DoubleStar, ISS Solaces, Proba-2, SOHO**, and **Ulysses**. The heliophysics files were originally created using [APIs/machine interfaces](https://csa.esac.esa.int/csa/aio/html/datarequests.shtml) and were thus first added to mission_info.toml with the format `api_v1`. However, there were multiple revisions requesting manual changes to them (changing the auto generated keywords, name schemas and some html functionality).  Therefore, the heliophysics files should not be overwritten by the script and only adapted manually, as the process cannot be recreated easily. If you want to do so anyway, do: 
- go to `resources/mission_info.toml` and set `crawled` to `false` for all helio missions you wish to change
- go to `resources/toml/heliophysics/[mission folder]/` and change the fields as you wish
- then, run `main.py`.
This can be useful when needing *only subparts* of the functionality, for example `post_process_data()`. Just comment out the other functions in `main.py`'s loop. 
## Astronomy 
Supported missions are **Lisa Pathfinder** and **GAIA**. The Astronomy branch uses a mix of the crawl formats `api_v2` and `manual`. The api_v2 is a work in progress. 
