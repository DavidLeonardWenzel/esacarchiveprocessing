
Note that for both PDS3 and PDS4 you can almost always "guess" where you have to go to find information by the structure of directories, but to do it "right" you should follow the meta-data trail, which is the way I've described below.


# PDS3 meta-data

PDS3 "labels" (meta-data) can be read with the python PVL library:

https://github.com/planetarypy/pvl   (also on conda-forge)

You can recursively scan each mission directory for the file VOLDESC.CAT which is in the top level directory of each dataset. This gives pointers to *other* labels which describe the instrument (INSTRUMENT_CATALOG), mission (MISSION_CATALOG) and dataset (DATA_SET_CATALOG) - these are all in the CATALOG folder. 

From these you should have everything you need, e.g.

* MISSION_NAME
* INSTRUMENT_NAME
* INSTRUMENT_TYPE
* DATA_SET_ID
* DATA_SET_NAME
* START_TIME
* STOP_TIME
* DATA_SET_DESC

I'm not sure what meta-data you need, but between these 3 catalogues probably you can find the essentials.


# PDS4

PDS4 is more well-constrained, but you have to dig around a bit more than PDS3. Also, not all of the meta-data that you need has been well-populated yet by the two missions using PDS4, so I would not assume anything is correct! But if you want to look at the structure, here we go:

Labels are all in XML, so you *can* use dedicated PDS4 tools, but since you are not interested in unpacking data or anything complex, you could also stick to basic XML tools (either the built-in XML library, or e.g. lxml in python).

PDS4 relies heavily on links between products, which are identified by a logical identifier (LID) and a version ID (VID) which are concatenated to make the unique ID with a double colon between them (LID::VID). Thus it is often easiest to index products first - basically scrape 

`//Identification_Area/logical_identifier`

and

`//Identification_Area/version_id`

from each label (I'm using Xpath notation). Actually you don't need to index all the product labels, so you can be much more restrictive (take the bundle and collection labels, so bundle*.xml and collection*.xml, and the contents of the mission bundle context collection and you should be OK).

The top level instrument "bundle" contains pointers to the mission description:

`//Context_Area/Investigation_Area[type eq "Mission"]/Internal_Reference/lid_reference`

which is a pointer to the LID of the mission context product, which itself has

`//Investigation/description`

so that's how you can retrieve mission information. Similarly in the bundle label you can get a pointer to the instrument description:

`//Context_Area/Observing_System[type eq "Instrument"]/Internal_Reference/lid_reference`

will take you there.

It also has pointer to each collection - look for Bundle_Member_Entry. Only take those with member_status=primary. The lidvid_reference references the collection that we need to index. The LID has a structure with : separated fields. The last part should tell you which directory within the bundle you need to look in.

e.g. for ExoMars2016 if you look at the bundle for em16_tgo_cas you will see a Bundle_Member_Entry pointing to a collection with LIDVID:

urn:esa:psa:em16_tgo_cas:data_raw::1.0

the last two parts are the bundle (em16_tgo_cas) and the collection (data_raw). Now you can go into the data_raw directory and look for collection*.xml. 

Anyway, if and when you get to PDS4, let me know!



* * * 

# Missions by PDS version

See the ftp (ftp://psa.esac.esa.int/pub/mirror/)

## PDS3

* CASSINI-HUYGENS/
* CHANDRAYAAN-1/
* EARTH/
* GIOTTO/
* HST/
* INTERNATIONAL-ROSETTA-MISSION/
* MARS-EXPRESS/
* SMALL-MISSIONS-FOR-ADVANCED-RESEARCH-AND-TECHNOLOGY/		
* VENUS-EXPRESS/

## PDS4

* BepiColombo/
* ExoMars2016/

