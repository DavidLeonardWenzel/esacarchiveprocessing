Fields we are looking for:
name, mission, url,
description, abstract
datePublished
temporalCoverage
instrument,
version,
missionDescription,
creatorContact


Observation -> Proposal ID
TAP:
	xmm-newton
	herschel
	hubble space telescope

SURVEYS:
	gaia - check back with juan 

LEGACY OBSERVATION:
	exosat 	  	
	iso
	planck - meet with monica

DIFF. CATEGORY (has tap, but needs to be defined)
	lisa pathfinder - meet with monica (possibly 1 per experiment)


---------------------CRAWLING--------------------
in TOPCAT
search for the mission
or use, for XMM:
http://nxsa.esac.esa.int/nxsa-web/#tap

proposal info
for each proposal_id: 
	from public.v_proposal_observation_info
	get those observation_id (for each prop ID)

for proposal ID in v_proposal:
	if proposal_id matches with proposal ID:
	pi_surname, pi_title 
		PI, abstract, title (for name)

for observation ID in public.v_exposure:
	if observation_id matches with proposal ID:
	instrument name, filter, mode_friendly_name, start time, stop time, 
	(lowest start time, longest end time)

for observation ID in v_public_observation:
	pps_version,proprietary_end_date + 1 as release date 




HST:
http://hst.esac.esa.int/tap-server/tap



--------------
planck:
 name -> Data ID
- the URL -> Link to the data all
- description & abstract
- date the data was published -> Date
- temporal coverage -> Mission start date (12 August 2009) and end date (23 October 2013)
- instrument: LFI and/or HFI
- version -> not applicable
- mission description ->
Planck is ESA's mission to observe the first light in the Universe. Planck was selected in 1995 as the third Medium-Sized Mission (M3) of ESA's Horizon 2000 Scientific Programme, and later became part of its Cosmic Vision Programme. It was designed to image the temperature and polarization anisotropies of the Cosmic Background Radiation Field over the whole sky, with unprecedented sensitivity and angular resolution. Planck is testing theories of the early universe and the origin of cosmic structure and providing a major source of information relevant to many cosmological and astrophysical issues.

https://www.cosmos.esa.int/web/planck/

- contact information (usually the PI): PLA Helpdesk https://support.cosmos.esa.int/pla/

--------------
LSP:
luis wants one per analysis object (28.802)
or one for each type of time analysis (5)
upd: the will send me info for 5 time analysis obj


integral: 
monc creates a view per proposal and gives me the link
i can then query it with tab
