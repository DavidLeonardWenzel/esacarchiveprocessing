schema org specification

I use https://schema.org/Dataset

Here, all possible fields I use are collected with their name and how 
they are marked up in schema.org format

header:
	<script type="application/ld+json
		"@context": "http://schema.org/",
		"@type" :"Dataset",

name
	
missions
DOI
instrument 
url
abstract
datePublished
keywords
publisherAndRegistrant
version (of data collection)
thumbnailURL	
audience 
temporalCoverage
description
missionDescription
creatorContact
license


Only For Table, not schema:
satelliteID, creditGuidelines

