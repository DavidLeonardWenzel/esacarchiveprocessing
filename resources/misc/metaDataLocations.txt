mission > instrument > latest version

[CATALOG]             Directory containing PDS catalog objects.
|  |
|  |-- CATINFO.TXT       Description of files in the CATALOG directory.
|  |-- DATASET.CAT       Description of the MIDAS data set.
|  |-- INST.CAT          Description of the MIDAS instrument.
|  |-- INSTHOST.CAT      Description of the ROSETTA spacecraft.
|  |-- MISSION.CAT       Description of the ROSETTA mission.
|  |-- REFERENCE.CAT     List of publications mentioned in catalog files.
|  |-- SOFTWARE.CAT      Description of provided S/W to read the data set.
|  +-- TARGET.CAT        Description of the ROSETTA mission targets.

AAREADME.TXT = Lead Scientist

in CATALOG:
CATINFO.TXT[OBJECT][PUBLICATION_DATE] 	publication date
DATASET.CAT[DAT_SET_DESC]				dataset description 
(needs truncating to only show overview, possibly processing and formating)

REF.CAT = 								related publication
REFERENCE.CAT

INST.CAT[OBJECT][INSTRUMENT_TYPE]		instrument type
INST.CAT[OBJECT][INSTRUMENT_NAME]		instrument name
INST.CAT[OBJECT][INSTRURMENT_DESC]		instrument description

MISSION.CAT 							mission overview

