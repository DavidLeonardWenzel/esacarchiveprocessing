import os
from datetime import datetime
import toml
import src.boundary.read_write as io
import src.control.misc as misc
import src.control.Spider as Spider
import src.control.Tf_idf as Tf_idf
import src.control.CollectionCreator as CollectionCreator
import src.entity.ExpChecklist as ExpChecklist
import src.entity.MetaData as MetaData
import src.entity.Warnings as Warnings

import requests
import json


def crawl(mission_info):
    """loads a checklist and if crawled == False,
    it crawls the experiment
    per exp, if succesfull, it writes a toml and sets crawled = True
    overwrites the checklist!"""
    spider = Spider.Spider(mission_info)
    spider.init()


def post_process_data(mission_info):
    """looks for description fields to create keywords"""
    tf_idf = Tf_idf.Tf_idf(mission_info)
    tf_idf.init()
    collectionCreator = CollectionCreator.CollectionCreator(mission_info)
    html, csv = collectionCreator.create()
    io.write(html, "resources/html/data/" +
             mission_info["science_branch"]+"/"+mission_info["name"]+"_index.html")
    io.write(csv, "resources/toml/" +
             mission_info["science_branch"]+"/"+mission_info["name"]+"_doi_registration_info.csv")


def folder_to_html(mission_info, inLoggingMode=True):
    """reads all tomls belonging to one mission and creates htmls with JSON-LD schema and DOI specifications"""
    folder_in = "resources/toml/" + \
        mission_info["science_branch"]+"/"+mission_info["name"]+"/"
    folder_out = "resources/html/data/" + \
        mission_info["science_branch"]+"/"+mission_info["name"]+"/"

    collection = io.get_all_docs(io.get_all_file_paths(folder_in))
    for exp in collection:
        warnings = Warnings.Warnings(exp)

        # CREATE CONTENT
        html = misc.create_html(exp, warnings, mission_info)

        # OUT FILE & LOG
        misc.ensure_write_safety(folder_out)
        io.write(html, folder_out+exp["name"]+".html")

        if inLoggingMode == True:
            time = datetime.now()
            timestamp = str(time.day) + "." + str(time.month) + "_" + str(time.hour) + \
                ":" + str(time.minute) + ":" + str(time.second) + \
                ":" + str
            io.write(warnings.report(), "resources/logs/log_"+timestamp+".txt")


def process_uncrawled_missions(debug=False):
    """if in debug, mission.toml does not get overwritten"""
    path = "resources/mission_info.toml"
    missions = io.read_toml(path)
    for mission in missions:
        if missions[mission]["crawled"] == False:
            print("Now working on uncrawled mission ",
                  missions[mission]["name"])
            if misc.crawl_type_is_supported(missions[mission]["name"], missions[mission]["format"]):
                crawl(missions[mission])
                post_process_data(missions[mission])
                folder_to_html(missions[mission], inLoggingMode=False)
                missions[mission]["crawled"] = True
    if not debug:
        io.write(toml.dumps(missions), path)


if __name__ == '__main__':
    process_uncrawled_missions()
